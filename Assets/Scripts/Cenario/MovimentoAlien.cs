﻿using UnityEngine;
using System.Collections;

public class MovimentoAlien : MonoBehaviour {

	private bool direcao = true;
	private float vel = 3;

	void Awake()
	{
		direcao = true;
		vel = 3;
	}

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (direcao)
		{
			transform.Translate(Vector2.right * Time.deltaTime * vel);
		}
		else
		{
			transform.Translate(Vector2.right * Time.deltaTime * vel * -1);
		}
	}

	void OnCollisionEnter2D(Collision2D coll)
	{
		if (coll.gameObject.name == "ParedeEsquerda")
		{
			direcao = true;
		}
		if (coll.gameObject.name == "ParedeDireita")
		{
			direcao = false;
		}
	}
}
