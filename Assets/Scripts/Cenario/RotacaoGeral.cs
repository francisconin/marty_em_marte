﻿using UnityEngine;
using System.Collections;

public class RotacaoGeral : MonoBehaviour {
	public Transform pivot;
	public float vel;

	// Use this for initialization
	void Start () 
	{

	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.RotateAround (pivot.position, Vector3.forward * -1, vel * Time.deltaTime);
	}
}
