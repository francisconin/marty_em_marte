﻿using UnityEngine;
using System.Collections;

public class Movimento : MonoBehaviour {
	public float vel = 3.0f; //velocidade padrao

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		// Horizontal e Vertical pegam as setas ou o WASD pra movimentar
		Vector2 x = Input.GetAxis("Horizontal") * transform.right * Time.deltaTime * vel;
		Vector2 y = Input.GetAxis("Vertical") * transform.up * Time.deltaTime * vel;

		//transform.Translate(Input.GetAxis("Horizontal"),Input.GetAxis("Vertical"), 0);	

		transform.Translate(x + y);

	}
}
