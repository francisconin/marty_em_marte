﻿using UnityEngine;
using System.Collections;

public class Inventory : MonoBehaviour 
{	
	public static int fuelAmount = 0;
	public Texture2D[] hudFuelAmount;
	public GUITexture fuelAmountHUDGUI;

	// Use this for initialization
	void Start () 
	{
		fuelAmount = 0;
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	void FuelPickup()
	{
		fuelAmount++;
		fuelAmountHUDGUI.texture = hudFuelAmount[fuelAmount];
	}
}
