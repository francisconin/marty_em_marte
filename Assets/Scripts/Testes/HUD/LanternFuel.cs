﻿using UnityEngine;
using System.Collections;

public class LanternFuel : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D Player)
	{
		Player.gameObject.SendMessage("FuelPickup");
		Destroy(this.gameObject);
	}

}
