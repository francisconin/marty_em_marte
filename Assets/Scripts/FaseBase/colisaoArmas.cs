﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class colisaoArmas : MonoBehaviour {
		
	//Declaraçao de variaveis

	private Animator _animator;
	private bool sugando = false;
	private GameObject _gosmaHandler;
	private qtdCapsulaArma3 _capsulaArma3;
	private qtdCapsulaArma2 _capsulaArma2;	
	private ScoreHandler _score;
	private streakHandler _streak;

	private AudioSource au_sugando;
	private AudioClip myAudioClip;
	private inputController _inputController;
		
	void Awake()
	{
		sugando = false;
	}

	void Start () 
	{		
		//Startando objetos com os componentes (scripts)
		_capsulaArma3 = GameObject.Find("barraArma3").GetComponent<qtdCapsulaArma3>();
		_capsulaArma2 = GameObject.Find("barraArma2").GetComponent<qtdCapsulaArma2>();
		_animator = GameObject.Find("Personagem").GetComponent<Animator>();
		_score = GameObject.Find("Score").GetComponent<ScoreHandler>();
		_streak = GameObject.Find("Streak").GetComponent<streakHandler>();
		_inputController = GameObject.Find("Main Camera").GetComponent<inputController>();
	

		au_sugando = (AudioSource)gameObject.AddComponent <AudioSource>();
		myAudioClip = (AudioClip)Resources.Load("SFX/PegandoGosma/PegandoGosma");
		//au_sugando.clip = myAudioClip;
		au_sugando.loop = false;
		au_sugando.playOnAwake = true;
	}

	void Update () 
	{


	}

	public bool getSugando()
	{
		return sugando;
	}

	void OnCollisionEnter2D(Collision2D coll) 
	{		
		if (coll.gameObject.tag == "Gosma")
		{		
			_score.setSugou();
			_streak.setSugou();
			au_sugando.PlayOneShot(myAudioClip, 0.25f);
			if (sugando)
			{
				Destroy(coll.gameObject);
				//Manda mensagem para o script qtdCapsulaArma3
				_capsulaArma3.gameObject.SendMessage("colidiuArma");
				//Manda mensagem para o script qtdCapsulaArma2
				_capsulaArma2.gameObject.SendMessage("colidiuArma");
				sugando = false;
             	_animator.SetBool("Absorb", false );
				//_animator.SetBool("Absorbt", false );
			}
		}
	}

	public void setSugando(bool status)
	{
		sugando = status;
		
		if (sugando)
		{
			_animator.SetBool("Absorb", true);
			//if (_inputController.movimentandoDireita)
			//{
			//	_animator.SetBool("Absorb", true);
			//}
			//if (_inputController.movimentandoEsquerda)
			//{
			//	_animator.SetBool("Absorb", true);
			//}
		}
		else		
		{
			//if(!_inputController.getArma2Ativa())
			_animator.SetBool("Absorb", false);
			//_animator.SetBool("Absorb", false);
		}
	}

}
