﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CriaGosmas : MonoBehaviour {
	
	//Declaraçao de variaveis

	public GameObject gosma;
	public float velMax = 1f;
	public float velMin = 0.7f;
	float respawnTimer;
	float startTimer = 4.0f;
    float timerTutoriais = 1.0f;
	float timerLentidao = 15.0f;
	private itemHandler _itens;
	private tutorialHandler _tutorial;

	private bool movimentou = false;
	private bool tutorialColetarFim = false;
    private bool tutorialMeteoroFim = false;
    private bool tutorialPortalFim = false;
    private bool tutorialUnicornioFim = false;

	private bool itemLentidaoAtivo = false;

	void Awake()
	{ 
		velMax = 1f;
		velMin = 0.7f;
		respawnTimer = Random.Range (0.7f, 1.2f);
		movimentou = false;
		startTimer = 4.0f;
        timerTutoriais = 1.0f;
		timerLentidao = 15.0f;
		itemLentidaoAtivo = false;
		tutorialColetarFim = false;
        tutorialMeteoroFim = false;
        tutorialPortalFim = false;
        tutorialUnicornioFim = false;
    }

	void Start () 
	{
		respawnTimer = Random.Range (0.7f, 1.2f);
		_itens = GameObject.Find("Main Camera").GetComponent<itemHandler>();
		_tutorial = GameObject.Find("Tutorial").GetComponent<tutorialHandler>();
	}

	void Update () 
	{
		//pega o valor dos itens, se estao ativos ou nao
		itemLentidaoAtivo = _itens.getItemLentidao();

		//se o item lentidao ta ativo, as gosmas caem lentas durante 15 segundos
		if (itemLentidaoAtivo)
		{
			if (timerLentidao <= 0)
			{
				velMax = 1f;
				velMin = 0.7f;
				timerLentidao = 15.0f;
				_itens.setItemLentidao(false);
			}
			else if (timerLentidao >= 0)
			{
				velMin = velMax = 0.2f;
				timerLentidao -= Time.deltaTime;
			}
		}		

		//Gosma nao pode nascer nos portais e nem na parede
		//if(Application.loadedLevelName == "sce_Fase4")
		//{
		//	respawnTimer -= Time.deltaTime;
		//	if (respawnTimer <= 0) 
		//	{
		//		gosma.GetComponent<Rigidbody2D>().gravityScale = Random.Range (velMin, velMax);
		//		if(Random.Range (-1, 1) >= 0) 
		//			gosma.transform.position = new Vector3(Random.Range (-6, -1), 6);
		//		else
		//			gosma.transform.position = new Vector3(Random.Range (1, 5), 6);
		//		Instantiate(gosma);
		//		respawnTimer = Random.Range (0.7f, 1.2f);
		//	}
        //}

        #region Fase 1
        if(Application.loadedLevelName == "sce_Fase1")
		{
            if (movimentou)
            {
                if (startTimer >= 0)
                {
                    startTimer -= Time.deltaTime;
                }
            }
						
			if (startTimer < 0)
			{
				if (startTimer <= 1.5f && !tutorialColetarFim)
				{
					respawnTimer -= Time.deltaTime;
					_tutorial.setTutorialColetar(true);
				}

				if (tutorialColetarFim)
				{
					Debug.Log("EntrouCriaGosma");
					Time.timeScale = 1;
					respawnTimer -= Time.deltaTime;
					if (respawnTimer <= 0) 
					{
						gosma.GetComponent<Rigidbody2D>().gravityScale = Random.Range (velMin, velMax);
						gosma.transform.position = new Vector3(Random.Range (-8, 5), 6);
						Instantiate(gosma);
						respawnTimer = Random.Range (0.7f, 1.2f);	
					}
				}
			}
        }

        #endregion  

        #region Fase 3
        else if (Application.loadedLevelName == "sce_Fase3")
        {
            timerTutoriais -= Time.deltaTime;
            if (timerTutoriais < 0)
            {
                if (timerTutoriais <= 1.5f && !tutorialMeteoroFim)
                {
                    Time.timeScale = 0;
                    respawnTimer -= Time.deltaTime;
                    _tutorial.setTutorialMeteoro(true);
                }

                if (tutorialMeteoroFim)
                {
                    Debug.Log("EntrouCriaGosma");
                    Time.timeScale = 1;
                    respawnTimer -= Time.deltaTime;
                    if (respawnTimer <= 0)
                    {
                        gosma.GetComponent<Rigidbody2D>().gravityScale = Random.Range(velMin, velMax);
                        gosma.transform.position = new Vector3(Random.Range(-8, 5), 6);
                        Instantiate(gosma);
                        respawnTimer = Random.Range(0.7f, 1.2f);
                    }
                }
            }
        }

        #endregion  

        #region Fase 4
        else if (Application.loadedLevelName == "sce_Fase4")
        {
            timerTutoriais -= Time.deltaTime;
            if (timerTutoriais < 0)
            {
                if (timerTutoriais <= 1.5f && !tutorialPortalFim)
                {
                    Time.timeScale = 0;
                    respawnTimer -= Time.deltaTime;
                    _tutorial.setTutorialPortal(true);
                }

                if (tutorialPortalFim)
                {
                    Time.timeScale = 1;
                    respawnTimer -= Time.deltaTime;
                    if (respawnTimer <= 0)
                    {
                        gosma.GetComponent<Rigidbody2D>().gravityScale = Random.Range(velMin, velMax);
                        if (Random.Range(-1, 1) >= 0)
                            gosma.transform.position = new Vector3(Random.Range(-6, -1), 6);
                        else
                            gosma.transform.position = new Vector3(Random.Range(1, 5), 6);
                        Instantiate(gosma);
                        respawnTimer = Random.Range(0.7f, 1.2f);
                    }
                }
            }
        }

        #endregion  

        #region Fase 5
        else if (Application.loadedLevelName == "sce_Fase5")
        {
            timerTutoriais -= Time.deltaTime;
            if (timerTutoriais < 0)
            {
                if (timerTutoriais <= 1.5f && !tutorialUnicornioFim)
                {
                    Time.timeScale = 0;
                    respawnTimer -= Time.deltaTime;
                    _tutorial.setTutorialUnicornio(true);
                }

                if (tutorialUnicornioFim)
                {
                    Time.timeScale = 1;
                    respawnTimer -= Time.deltaTime;
                    if (respawnTimer <= 0)
                    {
                        gosma.GetComponent<Rigidbody2D>().gravityScale = Random.Range(velMin, velMax);
                        gosma.transform.position = new Vector3(Random.Range(-8, 5), 6);
                        Instantiate(gosma);
                        respawnTimer = Random.Range(0.7f, 1.2f);
                    }
                }
            }
        }

        #endregion  

        else
		{
			respawnTimer -= Time.deltaTime;
			
			if (respawnTimer <= 0) 
			{
				gosma.GetComponent<Rigidbody2D>().gravityScale = Random.Range (velMin, velMax);
				gosma.transform.position = new Vector3(Random.Range (-8, 5), 6);
				Instantiate(gosma);
				respawnTimer = Random.Range (0.7f, 1.2f);	
			}			
		}
	}

	public void setMovimentando(bool status)
	{
		movimentou = status;
	}

	public void setTutorialColetarFim(bool status)
	{
		tutorialColetarFim = status;
	}

    public void setTutorialMeteoroFim(bool status)
    {
        tutorialMeteoroFim = status;
    }

    public void setTutorialPortalFim(bool status)
    {
        tutorialPortalFim = status;
    }

    public void setTutorialUnicornioFim(bool status)
    {
        tutorialUnicornioFim = status;
    }
}
