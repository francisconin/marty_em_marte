﻿using UnityEngine;
using System.Collections;

public class pauseCanvas : MonoBehaviour {

	//Declaraçao de variaveis
	public bool capsulaCheia = false;
	public Canvas pause;
	private Timer _textoTimer;

	void Awake()
	{
		capsulaCheia = false;
	}


	// Use this for initialization
	void Start () 
	{
		_textoTimer = GameObject.Find("ContadorText").GetComponent<Timer>();
		pause = GameObject.Find("CanvasButton").GetComponent<Canvas>();
	}
	
	// Update is called once per frame
	void Update () 
	{		
		if (capsulaCheia)
		{
			Time.timeScale = 0;
			pause.enabled = true;
			_textoTimer.setCapsulaCheia(true);
		}
		if (!capsulaCheia)
		{
			Time.timeScale = 1;
			pause.enabled = false;
			_textoTimer.setCapsulaCheia(false);
		}		
	}

	public void setPause(bool status)
	{
		capsulaCheia = status;
	}
}
