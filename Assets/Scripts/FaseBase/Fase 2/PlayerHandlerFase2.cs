﻿using UnityEngine;
using System.Collections;

public class PlayerHandlerFase2 : MonoBehaviour {
	
	//Declaraçao de variaveis
	
	public float vel = 9.0f; //velocidade padrao
	public GameObject objPersonagem;
	public GameObject moon; 
	
	private bool ClicouDir;
	private bool ClicouEsq;
	private bool maxEsquerda = false;
	private bool maxDireita = false;
	private GameObject PortalLaranja;
	private GameObject PortalAzul;
	
	private ScoreHandler _score;
	//private Rigidbody2D rb;
	
	void Awake()
	{ 
		vel = 9.0f; 
		maxEsquerda = false;
		maxDireita = false;
	}
	
	void Start () 
	{
		//Startando objetos com os componentes (scripts)
		PortalAzul = GameObject.Find("PortalAzul");
		PortalLaranja = GameObject.Find("PortalLaranja");
		moon = GameObject.Find("moon");
		_score = GameObject.Find("Score").GetComponent<ScoreHandler>();
		//rb = new Rigidbody2D();
	}
	
	void Update () 
	{
		bool movimento = false;
		
		//transform.Translate(Vector2.right * Time.deltaTime * 0.5f);

		if (ClicouDir && !maxDireita)
		{
			moon.transform.Rotate(new Vector3(0, 0, 0.1f));
			transform.Translate(Vector2.right * Time.deltaTime * vel);
			//rb.AddForce(Vector2.right * 300);
			movimento = true;
			maxEsquerda = false;
		} 
		if (ClicouEsq && !maxEsquerda)
		{ 
			moon.transform.Rotate(new Vector3(0, 0, -0.1f));
			transform.Translate((Vector2.right * Time.deltaTime * vel) * -1);
			movimento = true;
			maxDireita = false;
		}
		if (!movimento)
		{
			ClicouDir = false;
			ClicouEsq = false;
			transform.Translate(0,0,0);
		}
	}
	
	public void setClickDireita(bool click)
	{
		ClicouDir = click;
	}
	
	public void setClickEsquerda(bool click)
	{
		ClicouEsq = click;
	}
	
	public Vector3 getPlayerVector3()
	{
		return objPersonagem.transform.position;
	}
	
	void OnTriggerEnter2D(Collider2D coll)
	{
		if (coll.gameObject.name == "PortalLaranja")
		{
			transform.position = new Vector3((PortalAzul.transform.position.x - 1),PortalAzul.transform.position.y,PortalAzul.transform.position.z);
		}
		
		if (coll.gameObject.name == "PortalAzul")
		{
			transform.position = new Vector3((PortalLaranja.transform.position.x + 1),PortalLaranja.transform.position.y,PortalLaranja.transform.position.z);
		}
	}
	
	void OnCollisionEnter2D (Collision2D coll)
	{
		if (coll.gameObject.name == "ParedeEsquerda" || coll.gameObject.name == "barreiraEsquerda")
		{
			maxEsquerda = true;
		} 
		else if (coll.gameObject.name == "ParedeDireita" || coll.gameObject.name == "barreiraDireita") 
		{
			maxDireita = true;
		}		
		if (coll.gameObject.tag == "Meteoro")
		{
			Destroy(coll.gameObject);			
			GameObject ControleJogo = GameObject.Find("ControleJogo");
			var scriptControle = ControleJogo.GetComponent("scrControleJogo") as scrControleJogo; scriptControle._pontuacao = _score.pontuacao;
			Application.LoadLevel("sce_Ranking");
		}
	}
}
