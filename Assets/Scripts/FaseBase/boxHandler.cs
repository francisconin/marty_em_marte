﻿using UnityEngine;
using System.Collections;

public class boxHandler : MonoBehaviour {

	public Sprite _sprite;
	public GameObject[] _item;

	private int itemRandom = 0;
	private bool caiu = false;
	private Color color;
	private bool instanciar = true;
	private astronautaHandler _astronauta;

	void Awake()
	{
		itemRandom = 0;
		caiu = false;
		instanciar = true;
	}

	// Use this for initialization
	void Start () 
	{
		itemRandom = Random.Range(1,3);
		color = this.gameObject.GetComponent<Renderer>().material.color;
		_astronauta = GameObject.Find("Astronauta(Clone)").GetComponent<astronautaHandler>();
	}

	// Update is called once per frame
	void Update () 
	{
		if(caiu)
		{
			color.a -= 0.005f;
			GetComponent<Renderer>().material.color = color;
			if (color.a <= 0)
			{
				Destroy(this.gameObject);
			}
		}
	}

	public void Cair ()
	{
		this.gameObject.GetComponent<Rigidbody2D>().isKinematic = false;
		gameObject.transform.parent = null;
	}

	//void OnCollisionEnter2D(Collision2D coll) 
	void OnTriggerEnter2D(Collider2D coll)
	{
		if (coll.gameObject.name == "chao")
		{
			this.gameObject.GetComponent<SpriteRenderer>().sprite = _sprite;
			this.gameObject.GetComponent<Rigidbody2D>().isKinematic = true;
			this.transform.Translate(0,0,0);
			_astronauta.caixaCair();

			for (int i = 0; i < 4; i++)
			{
				if (itemRandom == i)
				{
					float posX = this.gameObject.transform.position.x;
					float posY = this.gameObject.transform.position.y	;
					_item[i].transform.position = new Vector2(posX, posY);
					caiu = true;
					if (instanciar)
					{
						Instantiate(_item[i]);
						instanciar = false;
					}
				}
			}
		}
	}
}
