﻿using UnityEngine;
using System.Collections;

public class inputController : MonoBehaviour {

	//Declaraçao de variaveis

	private bool arma2Ativa = false;
	private bool clickZerado = false;
	private Animator _animator;
	private bool isMobile = true;
	private playerHandler _player;
	private qtdCapsulaArma3 _capsula3;
	private qtdCapsulaArma2 _capsula2;
	private ScoreHandler _score;
	private CriaGosmas _criaGosmas;
	private itemHandler _itemHandler;
	private Transform target;
	private string rayObjeto;
	private float centroX;
	private float mouseX;
	private Vector3 objCentro;
	public bool movimentandoDireita = false; //checa no colisaoArmas
	public bool movimentandoEsquerda = false;//checa no colisaoArmas

	private AudioSource au_efeitoArma2;
	private AudioClip cl_inicioEfeito;
	private AudioClip cl_efeitoLoop;



	void Awake()
	{
		arma2Ativa = false;	
		isMobile = true;
		clickZerado = false;
		movimentandoDireita = false;
		movimentandoEsquerda = false;
	}

	
	void Start () 
	{  		
		//Verificaçao do tipo de dispositivo de entrada
		if (Application.isEditor)
			isMobile = false;
		
		//Startando objetos com os componentes (scripts)
		_animator = GameObject.Find("Personagem").GetComponent<Animator>();
		_player = GameObject.Find("Personagem").GetComponent<playerHandler>();
		_capsula3 = GameObject.Find("barraArma3").GetComponent<qtdCapsulaArma3>();
		_capsula2 = GameObject.Find("barraArma2").GetComponent<qtdCapsulaArma2>();
		_score = GameObject.Find("Score").GetComponent<ScoreHandler>();
		_criaGosmas = GameObject.Find("Criador de Gosma").GetComponent<CriaGosmas>();
		_itemHandler = GameObject.Find("Main Camera").GetComponent<itemHandler>();


		au_efeitoArma2 = (AudioSource)gameObject.AddComponent <AudioSource>();
		cl_inicioEfeito = (AudioClip)Resources.Load("SFX/Poder Arma 2/InicioPoder");
		cl_efeitoLoop = (AudioClip)Resources.Load("SFX/Poder Arma 2/poderArma2");
		au_efeitoArma2.clip = cl_efeitoLoop;
		au_efeitoArma2.loop = true;
		au_efeitoArma2.playOnAwake = false;
		
	}

	void Update () 
	{
		
		mouseX = Input.mousePosition.x;
		rayObjeto = CastRay();

		#region Setar no Score se sugou com a arma 1 ou 2
		if (arma2Ativa)
		{
			_score.setArma2Ativa(true);
		}
		if (!arma2Ativa)
		{
			_score.setArma2Ativa(false);
		}
		#endregion

		#region Astronauta
		
		//Caso clique no astronauta
		if(rayObjeto == "box" && Input.GetMouseButtonDown(0))
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit2D hit = Physics2D.Raycast (ray.origin, ray.direction, Mathf.Infinity);
			//Envia uma mensagem para a caixa cair
			hit.collider.gameObject.SendMessage("Cair");
		}

		if(rayObjeto == "Astronauta(Clone)" && Input.GetMouseButtonDown(0))
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit2D hit = Physics2D.Raycast (ray.origin, ray.direction, Mathf.Infinity);
			//Envia uma mensagem para a caixa cair
			hit.collider.gameObject.SendMessage("caixaCair");
		}
		#endregion

		#region Fase 5 (Unicornio)

		//Confundi a parte do unicornio com o meteoro (ignorem isso por enquanto, pois vou usar mais tarde) //Victor

		//Caso clique no unicorni
		if(rayObjeto == "Unicornio(Clone)" && Input.GetMouseButtonDown(0))
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit2D hit = Physics2D.Raycast (ray.origin, ray.direction, Mathf.Infinity);
			//Envia uma mensagem para ele ser destruido
			hit.collider.gameObject.SendMessage("Destrua");
		}
		#endregion

		#region Checar Vitoria
		
		if (rayObjeto == "Personagem" && Input.GetMouseButtonDown(0) && _capsula3.qtdGosmas >= 36) //Se a capsula verde estiver cheia, ou seja estiver na ultima sprite da capsula
		{
			GameObject ControleJogo = GameObject.Find("ControleJogo");
			var scriptControle = ControleJogo.GetComponent("scrControleJogo") as scrControleJogo; scriptControle._pontuacao = _score.pontuacao;
			Application.LoadLevel("sce_Ranking");
		}

		#endregion

		#region Movimentaçao
		
		//Movimentaçao por toque na tela, pra mobile e pra PC

		//Caso esteja rodando em plataforma mobile
		if(isMobile)
		{
			int tmp = Input.touchCount;
			tmp--;
			
			//Touch na direita da tela, move o personagem para a direita
			if((Input.GetTouch(tmp).phase == TouchPhase.Began))
			{
				//Avisa que a movimentaçao começou, pro jogo começar de verdade
				_criaGosmas.setMovimentando(true);
				_capsula2.setMovimentando(true);

				if (Input.GetTouch(tmp).position.x >= ConvertePosicaoObjeto(_player.getPlayerVector3()).x && Time.timeScale == 1)
				{
					_player.setClickDireita(true);
					_player.setClickEsquerda(false);
					movimentandoDireita = true;
					movimentandoEsquerda = false;
					_animator.SetBool("Walking_Right", true );
					_animator.SetBool("Walking_Left", false );
					_player.setFootstepsON();
				}
			}
			
			//Touch na esquerda da tela, move o personagem para a esquerda
			if((Input.GetTouch(tmp).phase == TouchPhase.Began))
			{
				//Avisa que a movimentaçao começou, pro jogo começar de verdade
				_criaGosmas.setMovimentando(true);
				_capsula2.setMovimentando(true);

				if (Input.GetTouch(tmp).position.x <= ConvertePosicaoObjeto(_player.getPlayerVector3()).x && Time.timeScale == 1)
				{
					_player.setClickEsquerda(true);
					_player.setClickDireita(false);
					movimentandoDireita = false;
					movimentandoEsquerda = true;
					
					_animator.SetBool("Walking_Right", false );
					_animator.SetBool("Walking_Left", true );
					_player.setFootstepsON();
				}
			}
			
			//Removido o dedo da tela, o personagem para.
			if(Input.GetTouch(tmp).phase == TouchPhase.Ended)
			{
				_player.setClickDireita(false);
				_player.setClickEsquerda(false);
				_animator.SetBool("Walking_Right", false );
				_animator.SetBool("Walking_Left", false );
				movimentandoDireita = false;
				movimentandoEsquerda = false;
				_player.setFootstepsOFF();
				
			}	
		}
		
		//Caso esteja rodando no PC
		else
		{
			//Clique na direita da tela, move o personagem para a direita
			if (mouseX >= ConvertePosicaoObjeto(_player.getPlayerVector3()).x && Time.timeScale == 1)
			{	
				if(Input.GetMouseButtonDown(0))
				{
					//Avisa que a movimentaçao começou, pro jogo começar de verdade
					_criaGosmas.setMovimentando(true);
					_capsula2.setMovimentando(true);


					_player.setClickDireita(true);
					_player.setClickEsquerda(false);
					movimentandoDireita = true;
					movimentandoEsquerda = false;
					_animator.SetBool("Walking_Right", true );
					_animator.SetBool("Walking_Left", false ); // nao sei
					_player.setFootstepsON();
				}
			}
			//Clique na esquerda da tela, move o personagem para a esquerda
			else if (mouseX <= ConvertePosicaoObjeto(_player.getPlayerVector3()).x && Time.timeScale == 1)
			{	
				if(Input.GetMouseButtonDown(0))
				{
					//Avisa que a movimentaçao começou, pro jogo começar de verdade
					_criaGosmas.setMovimentando(true);
					_capsula2.setMovimentando(true);


					_player.setClickEsquerda(true);
					_player.setClickDireita(false);
					movimentandoDireita = false;
					movimentandoEsquerda = true;
					_animator.SetBool("Walking_Left", true );
					_animator.SetBool("Walking_Right", false ); // nao sei
					_player.setFootstepsON();
				}
			}
			
			if(Input.GetMouseButtonUp(0))
			{
				_player.setClickDireita(false);	
				_animator.SetBool("Walking_Right", false );
				_player.setClickEsquerda(false);
				_animator.SetBool("Walking_Left", false );
				movimentandoDireita = false;
				movimentandoEsquerda = false;
				_player.setFootstepsOFF();
			}
		}
		#endregion
		
		#region Trocar para a Arma 2
		//para o som do efeito da arma 2
		if (arma2Ativa == false)
		{
			au_efeitoArma2.Stop();
		}
				
		//Troca para a Arma 2

		bool itemArma2Ativo = _itemHandler.getItemArma2();
		
		if (clickZerado && _capsula2.qtdGosmas >= 27 || itemArma2Ativo) //Confere se a capsula azul do personagem estiver cheia
		{
			arma2Ativa = true;

			//Inicia o som do efeito da arma
			au_efeitoArma2.PlayOneShot(cl_inicioEfeito, 0.25f);
			au_efeitoArma2.Play();

			//Reinicia a capsula azul do personagem
			_capsula2.qtdGosmas = 1;

			Animator animRender = GameObject.Find("AnimationControllerRendererArma2").GetComponent<Animator>();
			_animator.runtimeAnimatorController = animRender.runtimeAnimatorController;
		}

		#endregion

		#region Evitar Colisao entre Armas e Gosmas

		//Define qual arma esta ativa
		if (arma2Ativa)
		{
			Physics2D.IgnoreLayerCollision (12,10, true); //layer 12 = Arma 1, layer 10 = Gosma
			Physics2D.IgnoreLayerCollision (13,10, false);//layer 13 = Arma 2, layer 10 = Gosma
		}
		if (!arma2Ativa)
		{
			Physics2D.IgnoreLayerCollision (12,10, false);
			Physics2D.IgnoreLayerCollision (13,10, true); 
		}

		#endregion

	}

	//Transforma as posiçoes globais de um objeto em posiçoes (plano cartesiano) da tela
	private Vector3 ConvertePosicaoObjeto(Vector3 obj) 
	{
		Vector3 screenPos = GetComponent<Camera>().WorldToScreenPoint(obj);
		return screenPos;
	}

	//Retorna o nome do objeto que o mouse esta encostando
	string CastRay() 
	{
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit2D hit = Physics2D.Raycast (ray.origin, ray.direction, Mathf.Infinity);
		if (hit) {
			return hit.collider.gameObject.name;
		}
		return null;
	}
	
	public void StopMovementAnimation ()
	{
		_animator.SetBool("Walking_Left", false );
		_animator.SetBool("Walking_Right", false );
		_animator.SetBool("Walking_Right_Absorb", false );
		_animator.SetBool("Walking_Left_Absorb", false );
	}

	public void setArma2Ativa(bool status)
	{
		arma2Ativa = status;
	}

	public bool getArma2Ativa()
	{
		return arma2Ativa;
	}

	public void setClickZerado (bool status)
	{
		clickZerado = status;
	}
}
