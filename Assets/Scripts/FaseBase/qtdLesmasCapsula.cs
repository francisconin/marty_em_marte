﻿
using UnityEngine;
using System.Collections;

public class qtdLesmasCapsula : MonoBehaviour {
	
	//Declaraçao de variaveis

	public float qtdGosmas = 0;	
	public Sprite[] sprites;

	private SpriteRenderer spriteRenderer;		
	private ScoreHandler _score;
	private Animator _raio;
	private tornarInvisivel _melecaMonstro;
	private Animator anim_meleca;
	private bool colidiu = false;
	private float timer = 0.3f;

	void Awake()
	{
		qtdGosmas = 0;	
		colidiu = false;
		timer = 0.3f;
	}

	void Start () 
	{
		//Verifica se o sprite renderer e nulo
		spriteRenderer = GetComponent<SpriteRenderer>(); 		
		if (spriteRenderer.sprite == null)  
		{
			spriteRenderer.sprite = sprites[0]; 
		}

		
		_score = GameObject.Find("Score").GetComponent<ScoreHandler>();
		_raio = GameObject.Find("Raio").GetComponent<Animator>();
		_melecaMonstro = GameObject.Find("melecaMonstro").GetComponent<tornarInvisivel>();
		anim_meleca = GameObject.Find("melecaMonstro").GetComponent<Animator>();
	}

	void Update () 
	{
		if (colidiu)
		{
			if (timer >= 0)
			{
				timer -= Time.deltaTime;
			}
			if (timer < 0)
			{
				_raio.SetBool("spark", false);
				colidiu = false;
				timer = 0.3f;
			}
		}

		if (qtdGosmas >= 7.2)
		{
			GameObject ControleJogo = GameObject.Find("ControleJogo");
			var scriptControle = ControleJogo.GetComponent("scrControleJogo") as scrControleJogo; scriptControle._pontuacao = _score.pontuacao;
			Application.LoadLevel("sce_Ranking");
		}
	}
	
	//Scripts que chamam esse metodo acrescentam o contador da capsula
	public void colidiuGosma()
	{
		_melecaMonstro.setInvisivel(true);
		qtdGosmas = qtdGosmas + 0.1f;

		_raio.SetBool("spark", true);
		colidiu = true;


		//Seta sprites da capsula da Arma 3
		for (int i = 0; i < 8; i++) 
		{
			if(qtdGosmas == i)
			{
				spriteRenderer.sprite = sprites[i];		
				break;
			}
			if (qtdGosmas > 4 && qtdGosmas < 8)
			{
				anim_meleca.SetBool("estagio2", true);
			}
		}	
	}
}