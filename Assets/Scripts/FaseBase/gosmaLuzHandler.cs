﻿using UnityEngine;
using System.Collections;

public class gosmaLuzHandler : MonoBehaviour {
	
	//Declaraçao de variaveis
	
	public float vel = 2f; //velocidade padrao
	public Animator _animator;
	public float timer = 0.2f;
	public float moveForce = 50f;
	public float swapTime = 0.3f;
	
	private bool colidiuChao = false;
	private Collider2D colliderArma;
	private Collider2D colliderArma2;
	private colisaoArmas _colisaoArmas;
	private colisaoArmas _colisaoArmas2;
	private bool entrouRange = false;
	private bool timerOK = false;
	
	private AudioSource au_impactochao;
	private AudioClip myAudioClip;
	private int audioRandom;
	private string aux;

	private bool movimentacaoAtiva = false;
	private float moveTimer;
	private streakHandler _streak;
	
	void Awake()
	{ 
		vel = 2f;
		colidiuChao = false;
		entrouRange = false;
		timerOK = false;
		timer = 0.2f;
		moveTimer = swapTime;
	}
	
	void Start () 
	{
		//Startando objetos com os componentes (scripts)
		colliderArma = GameObject.Find("ColliderArma").GetComponent<BoxCollider2D>();
		colliderArma2 = GameObject.Find("ColliderArma2").GetComponent<PolygonCollider2D>();
		_colisaoArmas = GameObject.Find("ColliderArma").GetComponent<colisaoArmas>();
		_colisaoArmas2 = GameObject.Find("ColliderArma2").GetComponent<colisaoArmas>();
		_animator = this.gameObject.GetComponent<Animator>();
		_streak = GameObject.Find("Streak").GetComponent<streakHandler>();
		
		audioRandom = Random.Range(1,3);
		
		au_impactochao = (AudioSource)gameObject.AddComponent <AudioSource>();
		
		for (int i = 1; i <= 3; i++)
		{
			if (i == audioRandom)
			{
				aux = audioRandom.ToString();
				myAudioClip = (AudioClip)Resources.Load("SFX/Gosma/ImpactoChao" + aux);
			}
		}
		au_impactochao.clip = myAudioClip;
		au_impactochao.loop = false;
		au_impactochao.volume = 0.45f;


		moveTimer = swapTime;
	}
	
	
	void Update () 
	{

		// Espera um determinado tempo no chao, definido pela variavel Timer, antes de começar a andar em direçao a capsula (direita)
		if (colidiuChao)
		{
			if (timer >= 0)
			{
				timer -= Time.deltaTime;
			}
			if (timer < 0)
			{
				timerOK = true;
			}
			if (timerOK)
			{
				//Segue para a capsula
				transform.Translate(Vector2.right * Time.deltaTime * vel);
			}
		}	
		else
		{
			moveTimer -= Time.deltaTime;
			Debug.Log (moveTimer);
			if(moveTimer < 0)
			{
				movimentacaoAtiva = true;
				moveTimer = swapTime;
			}
			
			if(movimentacaoAtiva)		
			{
				if(Random.Range(0,2) == 1 && this.transform.position.x < 5)
					this.transform.Translate(Vector2.right * Time.deltaTime * moveForce);
				else if(this.transform.position.x > -7)
					this.transform.Translate(Vector2.right * Time.deltaTime * -moveForce);
				else
					this.transform.Translate(Vector2.right * Time.deltaTime * moveForce);
				movimentacaoAtiva = false;
			}
		}
		
		if (_colisaoArmas.getSugando())
		{			
			//Ativa a colisao de todas as armas com a gosma 
			//******O script inputController gerencia qual arma ira colidir com a gosma, na regiao de Evitar Colisao entre Armas e Gosmas *************
			Physics2D.IgnoreCollision(colliderArma, this.gameObject.GetComponent<BoxCollider2D>(), false);
			Physics2D.IgnoreCollision(colliderArma, this.gameObject.GetComponent<CircleCollider2D>(), false);
			Physics2D.IgnoreCollision(colliderArma2, this.gameObject.GetComponent<BoxCollider2D>(), false);
			Physics2D.IgnoreCollision(colliderArma2, this.gameObject.GetComponent<CircleCollider2D>(), false);
		}
		else if (!_colisaoArmas.getSugando())
		{			
			//Ignora a colisao de todas as armas com a gosma
			Physics2D.IgnoreCollision(colliderArma, this.gameObject.GetComponent<BoxCollider2D>(), true);
			Physics2D.IgnoreCollision(colliderArma, this.gameObject.GetComponent<CircleCollider2D>(), true);
			Physics2D.IgnoreCollision(colliderArma2, this.gameObject.GetComponent<BoxCollider2D>(), true);
			Physics2D.IgnoreCollision(colliderArma2, this.gameObject.GetComponent<CircleCollider2D>(), true);
		}
		
		
		//Verifica se a gosma esta acima do personagem para que o mesmo possa absorve-la //Com a Arma1
		if((_colisaoArmas.gameObject.transform.position.y > this.gameObject.transform.position.y - 3f &&
		    _colisaoArmas.gameObject.transform.position.y < this.gameObject.transform.position.y) &&
		   (_colisaoArmas.gameObject.transform.position.x > this.gameObject.transform.position.x - 1f &&
		 _colisaoArmas.gameObject.transform.position.x < this.gameObject.transform.position.x + 0.5f))
		{			
			_colisaoArmas.setSugando(true);
			entrouRange = true;
		}
		//Caso a gosma passe o personagem, o mesmo nao podera absorve-la //Com a Arma1
		else if(_colisaoArmas.gameObject.transform.position.y < this.gameObject.transform.position.y + 1.0f &&	
		        entrouRange)
		{
			_colisaoArmas.setSugando(false);
		}
		
		//Verifica se a gosma esta acima do personagem para que o mesmo possa absorve-la //Com a Arma2		
		if((_colisaoArmas2.gameObject.transform.position.y > this.gameObject.transform.position.y - 3f &&
		    _colisaoArmas2.gameObject.transform.position.y < this.gameObject.transform.position.y) &&
		   (_colisaoArmas2.gameObject.transform.position.x > this.gameObject.transform.position.x - 3f &&
		 _colisaoArmas2.gameObject.transform.position.x < this.gameObject.transform.position.x + 2f))
		{			
			_colisaoArmas2.setSugando(true);
			entrouRange = true;
		}
		//Caso a gosma passe o personagem, o mesmo nao podera absorve-la //Com a Arma2	
		else if(_colisaoArmas2.gameObject.transform.position.y < this.gameObject.transform.position.y + 1.0f &&	
		        entrouRange)
		{
			_colisaoArmas2.setSugando(false);
		}
	}
	
	void OnTriggerEnter2D(Collider2D capsula)
	{
		if (capsula.gameObject.name == "capsulaMeter")
		{
			//Manda mensagem para o script qtdLesmasCapsula
			capsula.gameObject.SendMessage("colidiuGosma");
			Destroy(this.gameObject);
		}
	}
	
	void OnCollisionEnter2D(Collision2D coll) 
	{
		if (coll.gameObject.name == "chao")
		{
			_streak.setZerou();
			au_impactochao.Play();
			colidiuChao = true;
			_animator.SetBool("CaiuChao", true );
		}
	}
}
