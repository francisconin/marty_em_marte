﻿using UnityEngine;
using System.Collections;

public class MeteoroHandler : MonoBehaviour {
	
	public float fallTimer = 5;
	private Shake cameraShake;
	private GameObject cameraObject;

	void Start () 
	{
		cameraObject = GameObject.Find("Main Camera");
		cameraShake = cameraObject.GetComponent(typeof(Shake)) as Shake;
	}

	void Update () 
	{		
		fallTimer -= Time.deltaTime;
		if(fallTimer > 0)
		{			
			this.GetComponent<Rigidbody2D>().gravityScale = 0;
							
			if(this.transform.position.x < GameObject.Find("Personagem").transform.position.x)
				this.transform.Translate(Vector2.right * Time.deltaTime * 1);
			else
				this.transform.Translate(Vector2.right * Time.deltaTime * -1);
		}
		else
		{
			this.GetComponent<Rigidbody2D>().gravityScale = 0.5f;
		}
	}
	
	public void Destrua()
	{		
		Destroy(this.gameObject);
	}
	
	void OnCollisionEnter2D(Collision2D coll) 
	{
		if (coll.gameObject.name == "chao")
		{			
			cameraShake.StartShake(Shake.ShakeType.custom);
			Destroy(this.gameObject);
		}
	}
}
