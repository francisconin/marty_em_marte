﻿using UnityEngine;
using System.Collections;

public class CriaMeteoro : MonoBehaviour {
	
	//Declaraçao de variaveis	
	public GameObject meteoro;
	public float vel;
	float respawnTimer;	
	
	void Awake()
	{ 
		vel = 0.5f;
		respawnTimer = Random.Range (2, 6);	
	}

	void Start () 
	{		
		respawnTimer = Random.Range (2, 6);	
	}

	void Update () 
	{	
		respawnTimer -= Time.deltaTime;
		if (respawnTimer <= 0) 
		{
			meteoro.GetComponent<Rigidbody2D>().gravityScale = vel;
			meteoro.transform.position = new Vector3(Random.Range (-8, 6), 4);
			Instantiate(meteoro);
			respawnTimer = Random.Range (2, 6);
		}
	}
}
