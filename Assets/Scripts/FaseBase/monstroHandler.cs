﻿using UnityEngine;
using System.Collections;

public class monstroHandler : MonoBehaviour {

	public Sprite spriteMonstro;
	private qtdLesmasCapsula _capsulaLesmas;

	// Use this for initialization
	void Start () 
	{
		//spriteMonstro = (Sprite)Resources.Load("Sprites/Monstro/Sprites_MONSTRAO");
		_capsulaLesmas = GameObject.Find("capsulaMeter").GetComponent<qtdLesmasCapsula>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (_capsulaLesmas.qtdGosmas >= 8)
		{
			this.gameObject.GetComponent<Animator>().enabled = false;
			this.gameObject.GetComponent<SpriteRenderer>().sprite = spriteMonstro;
			this.gameObject.transform.localScale = new Vector3 (0.8f,0.8f,0.8f);
		}
	}
}
