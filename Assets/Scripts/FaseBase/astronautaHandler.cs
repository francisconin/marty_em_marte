﻿using UnityEngine;
using System.Collections;

public class astronautaHandler : MonoBehaviour {

	public GameObject astronauta;

	private boxHandler _box;
	private CriaAstronauta _criaAstronauta;
	private bool toUp = false;
	private bool cantoTela = false;
	private bool caixaCaiu = false;

	void Awake ()
	{
		bool toUp = false;
     	bool cantoTela = false;
		bool caixaCaiu = false;
	}

	// Use this for initialization
	void Start () 
	{
		_box = GameObject.Find("box").GetComponent<boxHandler>();
		_criaAstronauta = GameObject.Find("CriaAstronauta").GetComponent<CriaAstronauta>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (caixaCaiu)
		{
			this.transform.Translate(Vector2.up * Time.deltaTime * 1.5f);
			if (this.transform.position.y > 7)
			{
				Destroy(astronauta);
			}
		}

		//Define se o astronauta chegou no canto da tela, pra fazer ele se direcionar ao outro canto
		if (this.gameObject.transform.position.x < -7)
		{
			cantoTela = true;
		}
		if (this.gameObject.transform.position.x > 7)
		{
			cantoTela = false;
		}

		//Faz o astronauta se mover para a direita ou esquerda, de acordo com o cantoTela
		if (!cantoTela && !caixaCaiu)
		{
			this.transform.Translate(Vector2.right * Time.deltaTime * -1.5f);
		}
		if (cantoTela && !caixaCaiu)
		{
			this.transform.Translate(Vector2.right * Time.deltaTime * 1.5f);
		}		

		if(this.transform.position.y <= 3)
		{			
			toUp = false;
		}
		else if(this.transform.position.y >= 4)
		{
			toUp = true;
		}
		
		if(toUp)
		{			
			this.transform.Translate(Vector2.up * Time.deltaTime * -0.5f);
		}
		else
		{
			this.transform.Translate(Vector2.up * Time.deltaTime * 0.5f);
		}
	}

	public void caixaCair()
	{
		_box.Cair();
		_criaAstronauta.resetRespawnTimer();
		caixaCaiu = true;
	}
}
