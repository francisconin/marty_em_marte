﻿using UnityEngine;
using System.Collections;

public class itemHandler : MonoBehaviour {

	private bool itemImaAtivo = false;
	private bool itemLentidao = false;
	private bool itemArma2 = false;

	private itemHandler _camera;

	void Awake()
	{
		itemImaAtivo = false;
		itemLentidao = false;
		itemArma2 = false;
	}

	// Use this for initialization
	void Start () 
	{
		_camera = GameObject.Find("Main Camera").GetComponent<itemHandler>();
	}
	
	// Update is called once per frame
	void Update () 
	{

	}


	void OnCollisionEnter2D(Collision2D coll) 
	{
		if (coll.gameObject.name == "Personagem")
		{
			if (this.gameObject.name == "itemIma(Clone)")
			{
				_camera.setItemIma(true);
				Destroy(this.gameObject);			
			}
			if (this.gameObject.name == "itemLentidao(Clone)")
			{
				_camera.setItemLentidao(true);
				Destroy(this.gameObject);			
			}
			if (this.gameObject.name == "itemArma2(Clone)")
			{
				_camera.setItemArma2(true);
				Destroy(this.gameObject);			
			}
		}
	}

	public void setItemIma (bool status)
	{
		itemImaAtivo = status;
	}
	
	public void setItemLentidao (bool status)
	{
		itemLentidao = status;
	}

	public void setItemArma2 (bool status)
	{
		itemArma2 = status;
	}

	public bool getItemIma()
	{
		return itemImaAtivo;
	}

	public bool getItemLentidao()
	{
		return itemLentidao;
	}

	public bool getItemArma2()
	{
		return itemArma2;
	}
}
