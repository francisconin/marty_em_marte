﻿using UnityEngine;
using System.Collections;

public class SmudgeHandler : MonoBehaviour {

	public GameObject smudge;
	private float timer = 2f;
	private Color color;

	void Awake()
	{
		timer = 2f;
	}

	// Use this for initialization
	void Start () 
	{
		color = this.gameObject.GetComponent<Renderer>().material.color;
		this.gameObject.transform.position = new Vector2 (0.59f, -3.18f);
	}
	
	// Update is called once per frame
	void Update () {
	
		if(smudge.GetComponent<Renderer>().enabled)
		{					
			//this.transform.Translate(Vector2.up * Time.deltaTime * -3);
			if (timer >= 0)
			{
				timer -= Time.deltaTime;
			}
			if (timer < 0)
			{
				if (color.a >= 0)
				{
					color.a -= 0.02f;
					GetComponent<Renderer>().material.color = color;
				}
			}

			if(color.a < 0)
				Destroy(this.gameObject);
		}
	}
}
