﻿using UnityEngine;
using System.Collections;

public class CriaUnicornio : MonoBehaviour {
	
	//Declaraçao de variaveis	
	public GameObject unicornio;
	public float vel;
	float respawnTimer;	
	
	void Awake()
	{ 
		vel = 0.5f;
		respawnTimer = Random.Range (2, 6);	
	}
	
	void Start () 
	{		
		respawnTimer = Random.Range (2, 6);	
	}
	
	void Update () 
	{	
		respawnTimer -= Time.deltaTime;
		if (respawnTimer <= 0) 
		{
			unicornio.transform.position = new Vector3(-14, Random.Range (-2, 3));
			Instantiate(unicornio);
			respawnTimer = Random.Range (2, 6);
		}
	}
}
