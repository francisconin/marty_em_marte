﻿using UnityEngine;
using System.Collections;

public class UnicornioHandler : MonoBehaviour {
	
	public GameObject unicornio;
	public GameObject smudge;
	
	private bool toUp = false;
	
	
	void Start () 
	{
		smudge.GetComponent<Renderer>().enabled = false;
	}
	
	void Update ()
	{
		this.transform.Translate(Vector2.right * Time.deltaTime * 5);
		
		if(this.transform.position.y <= -3)
		{			
			toUp = false;
		}
		else if(this.transform.position.y >= 4)
		{
			toUp = true;
		}
		
		if(toUp)
		{			
			this.transform.Translate(Vector2.up * Time.deltaTime * -5);
		}
		else
		{
			this.transform.Translate(Vector2.up * Time.deltaTime * 5);
		}
		
		
		if(this.transform.position.x > 0)
		{
			Destroy(unicornio);
			smudge.transform.position = new Vector3(0, 0);
			smudge.GetComponent<Renderer>().enabled = true;
			Instantiate(smudge);
		}
	}
	
	void Destrua()
	{
		Destroy(this.gameObject);
	}
}
