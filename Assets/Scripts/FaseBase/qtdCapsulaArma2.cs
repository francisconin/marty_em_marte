﻿using System.Collections;
using UnityEngine;

public class qtdCapsulaArma2 : MonoBehaviour {
		
	//Declaraçao de variaveis

	public float qtdGosmas = 0;	
	public Sprite[] sprites;	

	private float timerCapsula = 4.0f;
	private float timerArma2 = 6.0f;			
	private SpriteRenderer spriteRenderer;
	private tornarInvisivel _botao;
	private pauseCanvas _canvas;
	private inputController _inputController;
	private Click _buttonClick;
	private itemHandler _itemHandler;
	private bool movimentou = false;
	

	void Awake()
	{ 
		qtdGosmas = 0;	
		timerCapsula = 4.0f;
		timerArma2 = 6.0f;
		movimentou = false;
	}

	void Start () 
	{				
		//Verifica se o sprite renderer e nulo
		spriteRenderer = GetComponent<SpriteRenderer>();	
		if (spriteRenderer.sprite == null)
		{
			spriteRenderer.sprite = sprites[0];
		}
		
		//Startando objetos com os componentes (scripts)
		_canvas = GameObject.Find("pauseCanvas").GetComponent<pauseCanvas>();
		_inputController = GameObject.Find("Main Camera").GetComponent<inputController>();
		_buttonClick = GameObject.Find("Button").GetComponent<Click>();
		_itemHandler = GameObject.Find("Main Camera").GetComponent<itemHandler>();
		
	}

	void Update () 
	{
		if (movimentou)
		{
			//A cada 3.0s aumenta em 2 a quantidade de gosmas da Arma 2
			if (!_inputController.getArma2Ativa())
			{
				if (timerCapsula > 0)
					{
						timerCapsula -= Time.deltaTime;
					}
				if (timerCapsula <= 0)
					{
						qtdGosmas += 2;
						timerCapsula = 4.0f;
					}
			}
		}
		//A Arma 2 ativa dura 6.0s
		if (_inputController.getArma2Ativa())
		{
			if (timerArma2 > 0)
				{
					timerArma2 -= Time.deltaTime;
					qtdGosmas = 1;
				}
			if (timerArma2 <= 0)
				{
					qtdGosmas = 1;
					_inputController.setArma2Ativa(false);
					Animator animator = GameObject.Find("Personagem").GetComponent<Animator>();				
					Animator teste = GameObject.Find("AnimationControllerRendererArma1").GetComponent<Animator>();
					animator.runtimeAnimatorController = teste.runtimeAnimatorController;
					timerArma2 = 6.0f;	
					_buttonClick.setContador();
				}
		}

		//Seta sprites da capsula da Arma 2
		for (int i = 0; i < 26; i++) 
		{
			bool itemArma2Ativo = _itemHandler.getItemArma2();
			if(qtdGosmas == 1)
			{
				spriteRenderer.sprite = sprites[0];
				_canvas.setPause(false);
				break;
			}
			else if(qtdGosmas >= 26 && !itemArma2Ativo)
			{
				_canvas.setPause(true);
				spriteRenderer.sprite = sprites[26];
				break;
			}
			else if((qtdGosmas > (i + 1) && qtdGosmas < (i + 2)) || qtdGosmas == i && !itemArma2Ativo)
			{
				spriteRenderer.sprite = sprites[i];		
				break;
			}
		}		
	}

	//Scripts que chamam esse metodo acrescentam o contador da capsula
	public void colidiuArma()
	{
		qtdGosmas = qtdGosmas + 1f;
	}

	public void setQtdGosma()
	{
		qtdGosmas = 1;
	}

	public void setMovimentando(bool status)
	{
		movimentou = status;
	}

	//stein gay
}

