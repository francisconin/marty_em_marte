﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class streakHandler : MonoBehaviour {
	
	private Text _text;
	private string valorStreak;
	private int contadorStreak;
	private int streakFinal;

	// Use this for initialization
	void Start () 
	{
		_text = this.gameObject.GetComponent<Text>();		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (contadorStreak > streakFinal)
		{
			streakFinal = contadorStreak;
		}

		valorStreak = contadorStreak.ToString();
		_text.text = valorStreak + "x";
	}

	public void setSugou ()
	{
		contadorStreak += 1;

		Canvas streak = GameObject.Find("CanvasStreak").GetComponent<Canvas>();
		streak.enabled = true;
	}
		
	public void setZerou ()
	{
		contadorStreak = 0;
	}
}
