﻿using UnityEngine;
using System.Collections;

public class gosmaHandler : MonoBehaviour {
	
	//Declaraçao de variaveis

	public float vel = 2f; //velocidade padrao
	private float velIma = 2.0f;
	public Animator _animator;
	public float timer = 0.2f;
	private float timerIma = 8.0f;

	private float timerTransformacao = 0.2f;
	private bool colidiuChao = false;
	private Collider2D colliderArma;
	private Collider2D colliderArma2;
	private colisaoArmas _colisaoArmas;
	private colisaoArmas _colisaoArmas2;
	private bool entrouRange = false;
	private bool timerOK = false;

	private itemHandler _itens;
	private bool itemImaAtivo = false;
	
	private AudioSource au_impactochao;
	private AudioClip myAudioClip;
	private int audioRandom;
	private string aux;

	public AudioSource au_lesmaAndando;
	private inputController _inputController;
	private streakHandler _streak;

	void Awake()
	{ 
		vel = 2f;
		colidiuChao = false;
		entrouRange = false;
		timerOK = false;
		timer = 0.2f;
		timerIma = 8.0f;
		timerTransformacao = 0.2f;
	}

	void Start () 
	{
		//Startando objetos com os componentes (scripts)
		colliderArma = GameObject.Find("ColliderArma").GetComponent<BoxCollider2D>();
		colliderArma2 = GameObject.Find("ColliderArma2").GetComponent<PolygonCollider2D>();
		_colisaoArmas = GameObject.Find("ColliderArma").GetComponent<colisaoArmas>();
		_colisaoArmas2 = GameObject.Find("ColliderArma2").GetComponent<colisaoArmas>();
		_inputController = GameObject.Find("Main Camera").GetComponent<inputController>();
		_animator = this.gameObject.GetComponent<Animator>();
		_streak = GameObject.Find("Streak").GetComponent<streakHandler>();		
		_itens = GameObject.Find("Main Camera").GetComponent<itemHandler>();

		audioRandom = Random.Range(1,3);

		au_impactochao = (AudioSource)gameObject.AddComponent <AudioSource>();

		for (int i = 1; i <= 3; i++)
		{
			if (i == audioRandom)
			{
				aux = audioRandom.ToString();
				myAudioClip = (AudioClip)Resources.Load("SFX/Gosma/ImpactoChao" + aux);
			}
		}

		au_impactochao.clip = myAudioClip;
		au_impactochao.loop = false;
		au_impactochao.volume = 0.45f;

		au_lesmaAndando = (AudioSource)gameObject.AddComponent <AudioSource>();
		AudioClip myAudioClip3;
		myAudioClip3 = (AudioClip)Resources.Load("SFX/GosmaAndando/GosmaAndando");
		au_lesmaAndando.clip = myAudioClip3;
		au_lesmaAndando.loop = true;
		au_lesmaAndando.playOnAwake = false;
		au_lesmaAndando.volume = 0.05f;
		au_lesmaAndando.pitch = 0.4f;
	}
	

	void Update () 
	{
		itemImaAtivo = _itens.getItemIma();

		//Se o item de ima tiver ativo, as gosmas seguem o personagem por 8 segundos
		if (timerIma >= 0)
		{
			timerIma -= Time.deltaTime;
			if (itemImaAtivo && !colidiuChao)
			{
				if(this.transform.position.x < GameObject.Find("Personagem").transform.position.x)
					this.transform.Translate(Vector2.right * Time.deltaTime * velIma * 1);
				else
					this.transform.Translate(Vector2.right * Time.deltaTime * velIma * -1);
			}
		}
		if (timerIma < 0)
		{
			_itens.setItemIma(false);
			timerIma = 8.0f;
		}


		// Espera um determinado tempo no chao, definido pela variavel Timer, antes de começar a andar em direçao a capsula (direita)
		if (_animator.GetBool("CaiuChao") == true)
		{
			if (timerTransformacao >= 0)
			{
				timerTransformacao -= Time.deltaTime;
			}
			if (timerTransformacao < 0)
			{
				_animator.SetBool("transformou", true);
			}
		}

		if (colidiuChao)
		{
			if (timer >= 0)
			{
				timer -= Time.deltaTime;
			}
			if (timer < 0)
			{
				timerOK = true;
			}
			if (timerOK)
			{
				//Segue para a capsula
				transform.Translate(Vector2.right * Time.deltaTime * vel);
				au_lesmaAndando.UnPause();
			}
		}		

		if(!_inputController.getArma2Ativa())
		{
			if (_colisaoArmas.getSugando())
			{			
				//Ativa a colisao da Arma 1 com a gosma 
				//******O script inputController gerencia qual arma ira colidir com a gosma, na regiao de Evitar Colisao entre Armas e Gosmas *************
				Physics2D.IgnoreCollision(colliderArma, this.gameObject.GetComponent<BoxCollider2D>(), false);
				Physics2D.IgnoreCollision(colliderArma, this.gameObject.GetComponent<CircleCollider2D>(), false);
				//Physics2D.IgnoreCollision(colliderArma2, this.gameObject.GetComponent<BoxCollider2D>(), false);
				//Physics2D.IgnoreCollision(colliderArma2, this.gameObject.GetComponent<CircleCollider2D>(), false);


			}
			else if (!_colisaoArmas.getSugando())
			{			
				//Ignora a colisao da Arma 1 com a gosma
				Physics2D.IgnoreCollision(colliderArma, this.gameObject.GetComponent<BoxCollider2D>(), true);
				Physics2D.IgnoreCollision(colliderArma, this.gameObject.GetComponent<CircleCollider2D>(), true);
				Physics2D.IgnoreCollision(colliderArma2, this.gameObject.GetComponent<BoxCollider2D>(), true);
				Physics2D.IgnoreCollision(colliderArma2, this.gameObject.GetComponent<CircleCollider2D>(), true);

			}
			
			//Verifica se a gosma esta acima do personagem para que o mesmo possa absorve-la //Com a Arma1
			if((_colisaoArmas.gameObject.transform.position.y > this.gameObject.transform.position.y - 6f &&
			    _colisaoArmas.gameObject.transform.position.y < this.gameObject.transform.position.y) &&
			   (_colisaoArmas.gameObject.transform.position.x > this.gameObject.transform.position.x - 2f &&
			 _colisaoArmas.gameObject.transform.position.x < this.gameObject.transform.position.x + 1.5f))
			{		
				_colisaoArmas.setSugando(true);
				entrouRange = true;
				
			}
			//Caso a gosma passe o personagem, o mesmo nao podera absorve-la //Com a Arma1
			else if(_colisaoArmas.gameObject.transform.position.y < this.gameObject.transform.position.y + 1.0f &&	
			        entrouRange)
			{
				_colisaoArmas.setSugando(false);
				entrouRange = false;
			}

		}
		else
		{
		
			if (_colisaoArmas2.getSugando())
			{			
				//Ativa a colisao da Arma 2 com a gosma 
				//******O script inputController gerencia qual arma ira colidir com a gosma, na regiao de Evitar Colisao entre Armas e Gosmas *************
				//Physics2D.IgnoreCollision(colliderArma, this.gameObject.GetComponent<BoxCollider2D>(), false);
				//Physics2D.IgnoreCollision(colliderArma, this.gameObject.GetComponent<CircleCollider2D>(), false);
				Physics2D.IgnoreCollision(colliderArma2, this.gameObject.GetComponent<BoxCollider2D>(), false);
				Physics2D.IgnoreCollision(colliderArma2, this.gameObject.GetComponent<CircleCollider2D>(), false);
				
				
			}
			else if (!_colisaoArmas2.getSugando())
			{			
				//Ignora a colisao da Arma 2 com a gosma
				Physics2D.IgnoreCollision(colliderArma, this.gameObject.GetComponent<BoxCollider2D>(), true);
				Physics2D.IgnoreCollision(colliderArma, this.gameObject.GetComponent<CircleCollider2D>(), true);
				Physics2D.IgnoreCollision(colliderArma2, this.gameObject.GetComponent<BoxCollider2D>(), true);
				Physics2D.IgnoreCollision(colliderArma2, this.gameObject.GetComponent<CircleCollider2D>(), true);
				
				_animator.SetBool("Absorb_Right", false);
				_animator.SetBool("Absorb_Left", false);
			}

			//Verifica se a gosma esta acima do personagem para que o mesmo possa absorve-la //Com a Arma2		
			if((_colisaoArmas2.gameObject.transform.position.y > this.gameObject.transform.position.y - 6f &&
			    this.gameObject.transform.position.y > 1.0f) &&
			   (_colisaoArmas2.gameObject.transform.position.x > this.gameObject.transform.position.x - 3f &&
			 _colisaoArmas2.gameObject.transform.position.x < this.gameObject.transform.position.x + 2f))
			{			
				_colisaoArmas2.setSugando(true);
				entrouRange = true;
			}
			
			//Caso a gosma passe o personagem, o mesmo nao podera absorve-la //Com a Arma2	
			else if(this.gameObject.transform.position.y <= -1.0f && entrouRange)
			{
				_colisaoArmas2.setSugando(false);
				//_animator.SetBool("Absorb", false);
				entrouRange = false;
			}

		}



	}

	void OnTriggerEnter2D(Collider2D capsula)
	{
		if (capsula.gameObject.name == "capsulaMeter")
		{
			//Manda mensagem para o script qtdLesmasCapsula

			capsula.gameObject.SendMessage("colidiuGosma");
			Destroy(this.gameObject);
		}
	}

	void OnCollisionEnter2D(Collision2D coll) 
	{
		if (coll.gameObject.name == "chao")
		{
			_streak.setZerou();
			au_impactochao.Play();
			colidiuChao = true;
			_animator.SetBool("CaiuChao", true );
			au_lesmaAndando.Play();
			au_lesmaAndando.Pause();
		}
	}
}
