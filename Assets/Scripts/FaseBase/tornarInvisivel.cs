﻿using UnityEngine;
using System.Collections;

public class tornarInvisivel : MonoBehaviour {
	
	//Declaraçao de variaveis

	public bool invisivel = false;
		
	void Awake()
	{ 
		invisivel = false;
	}

	void Start () 
	{

	}	

	void Update () 
	{
		this.gameObject.GetComponent<Renderer>().enabled = invisivel;
	}

	public void setInvisivel (bool status)
	{
		invisivel = status;
	}
}
