﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreHandler : MonoBehaviour {

	public int pontuacao;

	private string score;
	private Text _text;
	private bool Arma2Ativa = false;

	void Awake()
	{
		score = "0";
		pontuacao = 0;
		Arma2Ativa = false;
	}

	// Use this for initialization
	void Start () 
	{
		_text = this.gameObject.GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		score = pontuacao.ToString();
		_text.text = "score: " + score;
	}

	public void setSugou ()
	{
		if (Arma2Ativa)
		{
			pontuacao += Random.Range (100,300);
		}
		if (!Arma2Ativa)
		{
			pontuacao += Random.Range (400,600);
		}
	}

	public void setArma2Ativa (bool status)
	{
		Arma2Ativa = status;
	}
}
