﻿using UnityEngine;
using System.Collections;

public class tutorialHandler : MonoBehaviour 
{
	private bool tutorialMovimentar = false;
	private bool tutorialColetar = false;
    private bool tutorialMeteoro = false;
    private bool tutorialPortal = false;
    private bool tutorialUnicornio = false;

	private bool tutorialMovimentarOnOFF = false;
	private bool tutorialColetarOnOFF = false;
    private bool tutorialMeteoroOnOFF = false;
    private bool tutorialPortalOnOFF = false;
    private bool tutorialUnicornioOnOFF = false;

	private bool timerOK = false;
	private Color color;
	private float timer = 5.5f;
	private float timerColetar = 3.5f;
	private contadorHandler _contador;
	private CriaGosmas _criaGosmas;

	public Sprite[] _sprite;

	void Awake()
	{
		tutorialMovimentar = false;
		tutorialColetar = false;
        tutorialMeteoro = false;
        tutorialPortal = false;
        tutorialUnicornio = false;

		timer = 5.5f;
		timerColetar = 3.5f;

		tutorialMovimentarOnOFF = false;
     	tutorialColetarOnOFF = false;
        tutorialMeteoroOnOFF = false;
        tutorialPortalOnOFF = false;
        tutorialUnicornioOnOFF = false;
	}

	void Start () 
	{
		color = this.gameObject.GetComponent<Renderer>().material.color;
		color.a = 0f;

		_contador = GameObject.Find("Contador").GetComponent<contadorHandler>();
		_criaGosmas = GameObject.Find("Criador de Gosma").GetComponent<CriaGosmas>();
	}

	void Update ()
    {
        if (Application.loadedLevelName == "sce_Fase1")
        {

            #region Tutorial Movimentar
            if (tutorialMovimentarOnOFF)
            {
                if (tutorialMovimentar)
                {
                    this.gameObject.GetComponent<SpriteRenderer>().sprite = _sprite[0];
                    this.gameObject.GetComponent<SpriteRenderer>().enabled = true;

                    if (color.a <= 1 && timer >= 0 && !timerOK)
                    {
                        color.a += 0.005f;
                        GetComponent<Renderer>().material.color = color;
                    }

                    if (color.a >= 1)
                    {
                        timerOK = true;
                    }

                    if (timerOK)
                    {
                        if (timer >= 0)
                        {
                            timer -= Time.unscaledDeltaTime;
                            GameObject.Find("dedo").GetComponent<Animator>().SetBool("dedo", true);
                            GameObject.Find("dedo").GetComponent<SpriteRenderer>().enabled = true;
                        }
                        if (timer < 0)
                        {
                            color.a -= 0.005f;
                            GetComponent<Renderer>().material.color = color;
                        }
                    }

                    if (color.a <= 0)
                    {
                        GameObject.Find("dedo").GetComponent<Animator>().SetBool("dedo", false);
                        GameObject.Find("dedo").GetComponent<SpriteRenderer>().enabled = false;
                        tutorialMovimentar = false;
                        _contador.setTutorial(false);
                        timer = 5.5f;
                        timerOK = false;
                    }
                }
            }
            else if (!tutorialMovimentarOnOFF)
            {
                GameObject.Find("dedo").GetComponent<Animator>().SetBool("dedo", false);
                GameObject.Find("dedo").GetComponent<SpriteRenderer>().enabled = false;
                tutorialMovimentar = false;
                _contador.setTutorial(false);
                timer = 5.5f;
                timerOK = false;
            }
            #endregion

            #region Tutorial Coletar
            if (tutorialColetarOnOFF)
            {
                if (tutorialColetar)
                {
                    Time.timeScale = 0;
                    this.gameObject.GetComponent<SpriteRenderer>().sprite = _sprite[1];
                    this.gameObject.GetComponent<SpriteRenderer>().enabled = true;

                    if (color.a <= 1 && timerColetar >= 0 && !timerOK)
                    {
                        color.a += 0.005f;
                        GetComponent<Renderer>().material.color = color;
                    }

                    if (color.a >= 1)
                    {
                        timerOK = true;
                    }

                    if (timerOK)
                    {
                        if (timerColetar >= 0)
                        {
                            timerColetar -= Time.unscaledDeltaTime;
                            GameObject.Find("dedo").GetComponent<Animator>().SetBool("dedo", true);
                            GameObject.Find("dedo").GetComponent<SpriteRenderer>().enabled = true;
                        }
                        if (timerColetar < 0)
                        {
                            color.a -= 0.005f;
                            GetComponent<Renderer>().material.color = color;
                        }
                    }

                    if (color.a <= 0)
                    {
                        GameObject.Find("dedo").GetComponent<Animator>().SetBool("dedo", false);
                        GameObject.Find("dedo").GetComponent<SpriteRenderer>().enabled = false;
                        tutorialColetar = false;
                        _criaGosmas.setTutorialColetarFim(true);
                        timerColetar = 3.5f;
                        timerOK = false;
                    }
                }
            }
            else if (!tutorialColetarOnOFF)
            {
                GameObject.Find("dedo").GetComponent<Animator>().SetBool("dedo", false);
                GameObject.Find("dedo").GetComponent<SpriteRenderer>().enabled = false;
                tutorialColetar = false;
                _criaGosmas.setTutorialColetarFim(true);
                timerColetar = 3.5f;
                timerOK = false;
            }

            #endregion
        }

        if (Application.loadedLevelName == "sce_Fase3")
        {
            #region Tutorial Meteoro
            if (tutorialMeteoroOnOFF)
            {
                if (tutorialMeteoro)
                {
                    Time.timeScale = 0;
                    this.gameObject.GetComponent<SpriteRenderer>().sprite = _sprite[2];
                    this.gameObject.GetComponent<SpriteRenderer>().enabled = true;

                    if (color.a <= 1 && timerColetar >= 0 && !timerOK)
                    {
                        color.a += 0.01f;
                        GetComponent<Renderer>().material.color = color;
                    }

                    if (color.a >= 1)
                    {
                        timerOK = true;
                    }

                    if (timerOK)
                    {
                        if (timerColetar >= 0)
                        {
                            timerColetar -= Time.unscaledDeltaTime;
                        }
                        if (timerColetar < 0)
                        {
                            color.a -= 0.01f;
                            GetComponent<Renderer>().material.color = color;
                        }
                    }

                    if (color.a <= 0)
                    {
                        tutorialMeteoro = false;
                        _criaGosmas.setTutorialMeteoroFim(true);
                        timerColetar = 3.5f;
                        timerOK = false;
                    }
                }
            }
            else if (!tutorialMeteoroOnOFF)
            {
                tutorialMeteoro = false;
                _criaGosmas.setTutorialMeteoroFim(true);
                timerColetar = 3.5f;
                timerOK = false;
            }

            #endregion
        }

        if (Application.loadedLevelName == "sce_Fase4")
        {
            #region Tutorial Portal
            if (tutorialPortalOnOFF)
            {
                if (tutorialPortal)
                {
                    Time.timeScale = 0;
                    this.gameObject.GetComponent<SpriteRenderer>().sprite = _sprite[3];
                    this.gameObject.GetComponent<SpriteRenderer>().enabled = true;

                    if (color.a <= 1 && timerColetar >= 0 && !timerOK)
                    {
                        color.a += 0.01f;
                        GetComponent<Renderer>().material.color = color;
                    }

                    if (color.a >= 1)
                    {
                        timerOK = true;
                    }

                    if (timerOK)
                    {
                        if (timerColetar >= 0)
                        {
                            timerColetar -= Time.unscaledDeltaTime;
                        }
                        if (timerColetar < 0)
                        {
                            color.a -= 0.01f;
                            GetComponent<Renderer>().material.color = color;
                        }
                    }

                    if (color.a <= 0)
                    {
                        tutorialPortal = false;
                        _criaGosmas.setTutorialPortalFim(true);
                        timerColetar = 3.5f;
                        timerOK = false;
                    }
                }
            }
            else if (!tutorialPortalOnOFF)
            {
                tutorialPortal = false;
                //_criaGosmas.setTutorialPortalFim(true);
                timerColetar = 3.5f;
                timerOK = false;
            }

            #endregion
        }

        if (Application.loadedLevelName == "sce_Fase5")
        {
            #region Tutorial Unicornio
            if (tutorialUnicornioOnOFF)
            {
                if (tutorialUnicornio)
                {
                    Time.timeScale = 0;
                    this.gameObject.GetComponent<SpriteRenderer>().sprite = _sprite[4];
                    this.gameObject.GetComponent<SpriteRenderer>().enabled = true;

                    if (color.a <= 1 && timerColetar >= 0 && !timerOK)
                    {
                        color.a += 0.01f;
                        GetComponent<Renderer>().material.color = color;
                    }

                    if (color.a >= 1)
                    {
                        timerOK = true;
                    }

                    if (timerOK)
                    {
                        if (timerColetar >= 0)
                        {
                            timerColetar -= Time.unscaledDeltaTime;
                        }
                        if (timerColetar < 0)
                        {
                            color.a -= 0.01f;
                            GetComponent<Renderer>().material.color = color;
                        }
                    }

                    if (color.a <= 0)
                    {
                        tutorialUnicornio = false;
                        _criaGosmas.setTutorialUnicornioFim(true);
                        timerColetar = 3.5f;
                        timerOK = false;
                    }
                }
            }
            else if (!tutorialUnicornioOnOFF)
            {
                tutorialUnicornio = false;
                //_criaGosmas.setTutorialPortalFim(true);
                timerColetar = 3.5f;
                timerOK = false;
            }

            #endregion
        }
    }

	public void setTutorialMovimentar(bool status)
	{
		tutorialMovimentar = status;
	}

	public void setTutorialColetar(bool status)
	{
		tutorialColetar = status;
	}

    public void setTutorialMeteoro(bool status)
    {
        tutorialMeteoro = status;
    }

    public void setTutorialPortal(bool status)
    {
        tutorialPortal = status;
    }

    public void setTutorialUnicornio(bool status)
    {
        tutorialUnicornio = status;
    }

	public void setTutorialMovimentarOnOFF (bool status)
	{
		tutorialMovimentarOnOFF = status;
	}

	public void setTutorialColetarOnOFF (bool status)
	{
		tutorialColetarOnOFF = status;
	}

    public void setTutorialMeteoroOnOFF(bool status)
    {
        tutorialMeteoroOnOFF = status;
    }

    public void setTutorialPortalOnOFF(bool status)
    {
        tutorialPortalOnOFF = status;
    }

    public void setTutorialUnicornioOnOFF(bool status)
    {
        tutorialUnicornioOnOFF = status;
    }
}
