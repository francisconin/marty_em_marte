﻿using UnityEngine;
using System.Collections;

public class CriaAstronauta : MonoBehaviour {

	//Declaraçao de variaveis	
	public GameObject astronauta;
	private float respawnTimer;	
	private bool instanciarOK = false;
	
	void Awake()
	{ 
		//vel = 0.5f;
		respawnTimer = 30;
	}
	
	void Start () 
	{		
		respawnTimer = 30;
	}
	
	void Update () 
	{	
		respawnTimer -= Time.deltaTime;
		if (respawnTimer <= 0 && !instanciarOK) 
		{
			astronauta.transform.position = new Vector3(11, 4);
			Instantiate(astronauta);
			instanciarOK = true;
		}
	}

	public void resetRespawnTimer()
	{
		respawnTimer = 30;
		instanciarOK = false;
	}
}
