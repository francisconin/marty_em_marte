﻿using UnityEngine;
using System.Collections;

public class contadorHandler : MonoBehaviour {
	private float timer;
	//private float timerTutorial = 5.5f;
	private int contador;
	private Color color;	
	//private Color colorTutorial;
	private bool tocou = false;
	private bool tutorial = false;
	private bool tutorialOK = true;
	private bool tutorialOnOFF = false;
	//private SpriteRenderer _spriteTutorial;

	public Sprite[] sprites;
	
	private AudioSource au_timer;
	private AudioClip cl_Base, cl_JA;

	private pauseCanvas _pause;
	private tutorialHandler _tutorial;


	void Awake()
	{
		contador = 0;
		tocou = false;
		tutorial = false;
		tutorialOK = true;
		//colorTutorial.a = 0;
		//timerTutorial = 5.5f;
	}

	// Use this for initialization
	void Start () 
	{
		contador = 0;
		color = this.gameObject.GetComponent<Renderer>().material.color;
		//colorTutorial = GameObject.Find("Tutorial").GetComponent<Renderer>().material.color;
		//colorTutorial.a = 0;
		//_spriteTutorial = GameObject.Find("Tutorial").GetComponent<SpriteRenderer>();

		
		_pause = GameObject.Find("pauseCanvas").GetComponent<pauseCanvas>();
		_tutorial = GameObject.Find("Tutorial").GetComponent<tutorialHandler>();
		
		au_timer = (AudioSource)gameObject.AddComponent <AudioSource>();
		cl_Base = (AudioClip)Resources.Load("SFX/Timer Inicial/timerBase");
		cl_JA = (AudioClip)Resources.Load("SFX/Timer Inicial/timerFinal");
	}
	
	// Update is called once per frame
	void Update () 
	{
		this.gameObject.GetComponent<SpriteRenderer>().enabled = true;

		if (tutorial)
		{
			_tutorial.setTutorialMovimentar(true);
			tutorialOK = false;
		}

		if (contador < 4)
		{
			Time.timeScale = 0;		
		

			if (contador == 0 && this.gameObject.GetComponent<SpriteRenderer>().sprite.name == "3_0")
			{
				color.a -= 0.015f;
				GetComponent<Renderer>().material.color = color;
				if (!tocou)
				{
					au_timer.PlayOneShot(cl_Base, 0.7f);
					tocou = true;
				}
				if (color.a < 0)
				{		
					this.gameObject.GetComponent<SpriteRenderer>().sprite = sprites[1];	
					color.a = 1;
					contador = 1;
					tocou = false;
				}
			}
			if (contador == 1 && this.gameObject.GetComponent<SpriteRenderer>().sprite.name == "2_0")
			{
				color.a -= 0.015f;
				GetComponent<Renderer>().material.color = color;
				if (!tocou)
				{
					au_timer.PlayOneShot(cl_Base, 0.7f);
					tocou = true;
				}
				if (color.a < 0)
				{
					this.gameObject.GetComponent<SpriteRenderer>().sprite = sprites[2];	
					color.a = 1;
					contador = 2;
					tocou = false;
				}
			}
			if (contador == 2 && this.gameObject.GetComponent<SpriteRenderer>().sprite.name == "1_0")
			{
				color.a -= 0.015f;
				GetComponent<Renderer>().material.color = color;
				if (!tocou)
				{
					au_timer.PlayOneShot(cl_Base, 0.7f);
					tocou = true;
				}
				if (color.a < 0)
				{
					this.gameObject.GetComponent<SpriteRenderer>().sprite = sprites[3];	
					color.a = 1;
					contador = 3;
					tocou = false;
				}
			}
			if (contador == 3 && this.gameObject.GetComponent<SpriteRenderer>().sprite.name == "JA_1")
			{
				color.a -= 0.02f;
				GetComponent<Renderer>().material.color = color;
				if (!tocou)
				{
					au_timer.PlayOneShot(cl_JA, 0.7f);
					tocou = true;
				}
				//Time.timeScale = 1;
				if (color.a < 0)
				{
					if (Application.loadedLevelName == "sce_Fase1" && tutorialOK && tutorialOnOFF)
					{
						tutorial = true;
					}
					if (!tutorial)
					{
						contador = 4;
						Time.timeScale = 1;
					}
				}
			}
		}
		else if (contador == 4)
		{
			_pause.setPause(false);
			contador += 1;
		}
	}

	public void setTutorial(bool status)
	{
		tutorial = status;
	}

	public void setTutorialOnOFF(bool status)
	{
		tutorialOnOFF = status;
	}
}
