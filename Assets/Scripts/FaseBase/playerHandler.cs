using UnityEngine;
using System.Collections;

public class playerHandler : MonoBehaviour {

	//Declaraçao de variaveis

	public float vel = 9.2f; //velocidade padrao
	public GameObject objPersonagem;
	public GameObject moon; 
	//Movimentaçao escorregadia - Fase 2 (Gelo)
	public float slide = 5f;
	public float slide_force = 5f;
	public float slide_slow = 0.001f;

	private bool passouPortalEsquerda = false;
	private bool ClicouDir;
	private bool ClicouEsq;
	private bool maxEsquerda = false;
	private bool maxDireita = false;
	private GameObject PortalLaranja;
	private GameObject PortalAzul;

	private float timer = 0.5f;
	
	private Animator _animator;
	
	private ScoreHandler _score;

	private AudioSource au_footsteps;
	//private Rigidbody2D rb;

	void Awake()
	{ 
		vel = 9.2f; 
		timer = 0.5f;
		maxEsquerda = false;
		maxDireita = false;
		passouPortalEsquerda = false;
	}

	void Start () 
	{
		//Startando objetos com os componentes (scripts)
		PortalAzul = GameObject.Find("PortalAzul");
		PortalLaranja = GameObject.Find("PortalLaranja");
		moon = GameObject.Find("moon");
		_score = GameObject.Find("Score").GetComponent<ScoreHandler>();
		//rb = new Rigidbody2D();

		au_footsteps = (AudioSource)gameObject.AddComponent <AudioSource>();
		AudioClip myAudioClip;
		myAudioClip = (AudioClip) Resources.Load("SFX/Marty/FOOTSTEP");
		au_footsteps.clip = myAudioClip;
		au_footsteps.loop = true;
		au_footsteps.pitch = 0.94f;

		_animator = this.gameObject.GetComponent<Animator>();
	}

	void Update () 
	{

		if (passouPortalEsquerda)
		{
		
			if (timer >= 0)
			{
				timer -= Time.deltaTime;
			}
			if (timer < 0)
			{
				_animator.SetBool("Portal_Left", false );
				//transform.position = new Vector3((PortalAzul.transform.position.x - 2),PortalAzul.transform.position.y,PortalAzul.transform.position.z);
				passouPortalEsquerda = false;
				timer = 0.5f;
			}
		}

		//Movimentaçao escorregadia
		if(Application.loadedLevelName == "sce_Fase2")
		{
			bool movimento = false;

			if(maxDireita || maxEsquerda)
				slide = 0f;
				transform.Translate(Vector2.right * Time.deltaTime * slide);

			if (ClicouDir && !maxDireita)
			{
				slide += slide_force;
				moon.transform.Rotate(new Vector3(0, 0, 0.1f));
				transform.Translate(Vector2.right * Time.deltaTime * vel);
				movimento = true;
				maxEsquerda = false;
			} 
			if (ClicouEsq && !maxEsquerda)
			{ 
				slide -= slide_force;
				moon.transform.Rotate(new Vector3(0, 0, -0.1f));
				transform.Translate((Vector2.right * Time.deltaTime * vel) * -1);
				movimento = true;
				maxDireita = false;
			}
			if (!movimento)
			{
				if(slide > 0)
					slide -= slide_slow;
				else					
					slide += slide_slow;
				ClicouDir = false;
				ClicouEsq = false;
				transform.Translate(0,0,0);
			}
		}
		else
		{
		bool movimento = false;

		if (ClicouDir && !maxDireita)
		{
			transform.Translate(Vector2.right * Time.deltaTime * vel);
			//rb.AddForce(Vector2.right * 300);
			movimento = true;
			maxEsquerda = false;
		} 
		if (ClicouEsq && !maxEsquerda)
		{ 
			transform.Translate((Vector2.right * Time.deltaTime * vel) * -1);
			movimento = true;
			maxDireita = false;
		}
		if (!movimento)
		{
			ClicouDir = false;
			ClicouEsq = false;
			transform.Translate(0,0,0);
		}
		}
	}

	public void setClickDireita(bool click)
	{
		ClicouDir = click;
	}

	public void setClickEsquerda(bool click)
	{
		ClicouEsq = click;
	}

	public void setFootstepsON()
	{
		au_footsteps.Play();
	}

	public void setFootstepsOFF()
	{
		au_footsteps.Stop();
	}


	public Vector3 getPlayerVector3()
	{
		return objPersonagem.transform.position;
	}

	void OnTriggerEnter2D(Collider2D coll)
	{
		if (coll.gameObject.name == "PortalLaranja")
		{
			_animator.SetBool("Portal_Left", true );
			passouPortalEsquerda = true;
			transform.position = new Vector3((PortalAzul.transform.position.x - 1),PortalAzul.transform.position.y - 0.3f ,PortalAzul.transform.position.z);
		}

		if (coll.gameObject.name == "Cube")
		{
			Debug.Log("Colidiu");
			_animator.SetBool("Portal_Left", true );
			passouPortalEsquerda = true;
		}

		if (coll.gameObject.name == "PortalAzul")
		{
			//_animator.SetBool("Portal_Right", true );
			transform.position = new Vector3((PortalLaranja.transform.position.x + 2),PortalLaranja.transform.position.y - 0.3f,PortalLaranja.transform.position.z);
		}
	}

	void OnCollisionEnter2D (Collision2D coll)
	{
		if (coll.gameObject.name == "ParedeEsquerda" || coll.gameObject.name == "barreiraEsquerda")
		{
			maxEsquerda = true;
		} 
		else if (coll.gameObject.name == "ParedeDireita" || coll.gameObject.name == "barreiraDireita") 
		{
			maxDireita = true;
		}		
		if (coll.gameObject.tag == "Meteoro")
		{
			Destroy(coll.gameObject);			
			GameObject ControleJogo = GameObject.Find("ControleJogo");
			var scriptControle = ControleJogo.GetComponent("scrControleJogo") as scrControleJogo; scriptControle._pontuacao = _score.pontuacao;
			Application.LoadLevel("sce_Ranking");
		}
	}
}
