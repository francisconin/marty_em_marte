﻿using UnityEngine;
using System.Collections;

public class tutorialOnOFF : MonoBehaviour 
{
	public bool tutorialMovimentar;
	public bool tutorialColetar;
    public bool tutorialMeteoro;
	public bool tutorialPortal;
    public bool tutorialUnicornio;

	private tutorialHandler _tutorial;
	private contadorHandler _contador;


	void Start () 
	{
		_tutorial = GameObject.Find("Tutorial").GetComponent<tutorialHandler>();
		_contador = GameObject.Find("Contador").GetComponent<contadorHandler>();
	}

	void Update () 
	{
		_tutorial.setTutorialMovimentarOnOFF(tutorialMovimentar);
		_tutorial.setTutorialColetarOnOFF(tutorialColetar);
        _tutorial.setTutorialMeteoroOnOFF(tutorialMeteoro);
        _tutorial.setTutorialPortalOnOFF(tutorialPortal);
        _tutorial.setTutorialUnicornioOnOFF(tutorialUnicornio);
		_contador.setTutorialOnOFF(tutorialMovimentar);
	}
}