﻿using UnityEngine;
using System.Collections;

public class FixarPosicao : MonoBehaviour {


	private Vector3 position;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (this.gameObject.name == "ParedeEsquerda")
		{
			position = transform.position;
			position.x = -9;
			transform.position = position;	
		}
		if (this.gameObject.name == "ParedeDireita")
		{
			position = transform.position;
			position.x = 9;
			transform.position = position;	
		}

		if (this.gameObject.name == "barreira")
		{
			position = transform.position;
			position.x = 0;
			transform.position = position;	
		}
		if (this.gameObject.name == "barreiraDireita")
		{
			position = transform.position;
			position.x = 0.2f;
			transform.position = position;	
		}
		if (this.gameObject.name == "barreiraEsquerda")
		{
			position = transform.position;
			position.x = -0.1f;
			transform.position = position;	
		}
	}
}
