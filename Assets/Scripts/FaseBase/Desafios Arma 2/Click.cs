﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Click : MonoBehaviour {

	private Text textoContador;
	private int num = 50;
	private bool timerZerado = false;
	private pauseCanvas _pause;
	private inputController _inputController;

	private Timer _timerDesafioClique;

	private AudioSource au_desafioClick;
	private AudioClip myAudioClip;

	void Awake()
	{
		num = 50;
	}

	// Use this for initialization
	void Start () 
	{
		textoContador = GameObject.Find("Text").GetComponent<Text>();
		_pause = GameObject.Find("pauseCanvas").GetComponent<pauseCanvas>();
		_inputController = GameObject.Find("Main Camera").GetComponent<inputController>();
		_timerDesafioClique = GameObject.Find("ContadorText").GetComponent<Timer>();


		au_desafioClick = (AudioSource)gameObject.AddComponent <AudioSource>();
		myAudioClip = (AudioClip)Resources.Load("SFX/DesafioClick/DesafioClick");
	}
	
	// Update is called once per frame
	void Update () 
	{
		string aux = num.ToString();
		textoContador.text = aux;


		if (num < 50 && num > 0)
		{
			_timerDesafioClique.setClickStart(true);
		}


		if (num <= 0)
		{
			_pause.setPause(false);
			_inputController.setClickZerado(true);
		}

	}

	void OnClick()
	{
		num--;
		au_desafioClick.PlayOneShot(myAudioClip, 0.4f);
	}

	public void setContador()
	{
		num = 50;
		timerZerado = false;
		_timerDesafioClique.setContador();
		_inputController.setClickZerado(false);
	}

	public void setTimerZerado(bool status)
	{
		timerZerado = status;
	}
}
