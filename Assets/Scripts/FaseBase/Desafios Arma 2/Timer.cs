﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Timer : MonoBehaviour {

	private double contador = 9.59f;
	private string aux;	
	private bool capsulaCheia = false;
	private bool clickStart = false;

	private Text texto;
	private Click _botaoDesafioClique;
	private qtdCapsulaArma2 _capsulaArma2;


	void Awake()
	{ 
		contador = 9.59f;
		capsulaCheia = false;
		clickStart = false;
	}

	// Use this for initialization
	void Start () 
	{		
		texto = this.gameObject.GetComponent<Text>();
		_botaoDesafioClique = GameObject.Find("Button").GetComponent<Click>();
		_capsulaArma2 = GameObject.Find("barraArma2").GetComponent<qtdCapsulaArma2>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (capsulaCheia)
		{
			if (contador < 0)
			{
				_botaoDesafioClique.setTimerZerado(true);
				_botaoDesafioClique.setContador();
				_capsulaArma2.setQtdGosma();
				contador = 9.59f;
			}

			else if (contador > -1)
			{			
				contador = System.Math.Round(contador, 2);
				aux = contador.ToString();
				texto.text = aux;
				if (clickStart)
				{
					contador -= Time.unscaledDeltaTime;			
				}
			}
		}
	}

	public void setCapsulaCheia(bool status)
	{
		capsulaCheia = status;
	}

	public void setContador()
	{
		contador = 9.59f;
	}

	public void setClickStart (bool status)
	{
		clickStart = status;
	}
}
