﻿using UnityEngine;
using System.Collections;

public class qtdCapsulaArma3 : MonoBehaviour {
	
	//Declaraçao de variaveis

	public float qtdGosmas = 0;
	public Sprite[] sprites;	

	private tornarInvisivel _seta;	
	private SpriteRenderer spriteRenderer;
		
	void Awake()
	{ 
		qtdGosmas = 0;
	}

	void Start () 
	{		
		//Verifica se o sprite renderer e nulo
		spriteRenderer = GetComponent<SpriteRenderer>(); 		
		if (spriteRenderer.sprite == null)  
		{
			spriteRenderer.sprite = sprites[0]; 
		}
		
		//Startando objetos com os componentes (scripts)
		_seta = GameObject.Find("seta_0").GetComponent<tornarInvisivel>();
		
	}

	void Update () 
	{
		if (qtdGosmas >= 39)
		{
			_seta.invisivel = true;
		}

		//Seta sprites da capsula da Arma 3
		for (int i = 0; i < 40; i++) 
		{
			if((qtdGosmas > i && qtdGosmas < (i + 1)) || qtdGosmas == i)
			{
				spriteRenderer.sprite = sprites[i];		
				break;
			}
		}	
	}

	//Scripts que chamam esse metodo acrescentam o contador da capsula
	public void colidiuArma()
	{
		qtdGosmas = qtdGosmas + 0.3f;		
	}
}
